{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE LambdaCase       #-}
{-# LANGUAGE NamedFieldPuns   #-}

module Lib where

import           Control.Lens             (Lens, (<%=))
import           Control.Lens.Combinators (Traversal', each, toListOf)
import           Control.Lens.Fold        ((^..))
import           Control.Lens.Getter      ((^.))
import           Control.Lens.Lens        ((&))
import           Control.Lens.Operators   ((.~), (^?))
import           Control.Lens.Traversal   (traverseOf)
import           Control.Lens.Type        (Traversal)
import           Control.Monad.Reader     (forever, liftIO)
import           Control.Monad.State      (StateT, evalStateT, get, gets,
                                           modify, put)
import           Data.Aeson.Lens          (AsValue, key, values, _String)
import           Data.Aeson.Types         (Value (..))
import qualified Data.ByteString.Char8    as C
import qualified Data.ByteString.Lazy     as LBS
import           Data.Maybe               (fromMaybe, isJust)
import           Data.Text                (Text)
import qualified Data.Text                as T
import           Network.Wreq             (Response, asJSON, asValue, defaults,
                                           getWith, responseBody)
import           Network.Wreq.Lens        (header, param)
import           System.Environment       (lookupEnv)
import           Web.Slack                (SlackHandle, getNextEvent,
                                           sendMessage, withSlackHandle)
import           Web.Slack.Types          (Event (..))
import           Web.Slack.WebAPI         (SlackConfig (..), _slackApiToken)

newtype CounterState =
    CounterState
        { messageCount :: Int
        }

data GitlabConfig =
    GitlabConfig
        { hostname :: Hostname
        , group    :: Group
        , token    :: Token
        }

type Mr = (Text, Text)

type Hostname = String

type Token = String

type Group = String

run :: IO ()
run = do
    maybeSlackToken <- lookupEnv "SLACK_TOKEN"
    maybeGroup <- lookupEnv "GITLAB_GROUP"
    maybeHostname <- lookupEnv "GITLAB_HOSTNAME"
    maybeGitlabToken <- lookupEnv "GITLAB_TOKEN"
    case (maybeSlackToken, maybeHostname, maybeGitlabToken, maybeGroup) of
        (Just slackToken, Just hostname, Just gitlabToken, Just group) -> do
            let config = SlackConfig {_slackApiToken = slackToken}
                gitlabConf = GitlabConfig hostname gitlabToken group
            putStrLn "Got config. Starting bot..."
            withSlackHandle config $ \h -> evalStateT (counterBot gitlabConf h) (CounterState 0)
            putStrLn "Done."
        _ -> fail "Please set SLACK_TOKEN, GITLAB_TOKEN, GITLAB_HOSTNAME, GITLAB_GROUP"

-- Count how many messages the bot receives
counterBot :: GitlabConfig -> SlackHandle -> StateT CounterState IO ()
counterBot config h =
    forever $
    liftIO (getNextEvent h) >>= \case
        msg@(Message cid _ _ _ _ _) -> do
            num <- (+ 1) <$> gets messageCount
            liftIO $ putStrLn $ "Got a message (#" <> show num <> "): " <> show msg
            put (CounterState num)
            mrs <- liftIO (getMRs config)
            let rendered = "*Open Merge Requests!*\n" <> T.unlines (renderMr <$> mrs)
            liftIO $ sendMessage h cid rendered
        msg -> liftIO $ print msg

renderMr :: Mr -> Text
renderMr (t, l) = " • <" <> l <> "|" <> t <> ">"

getMRs :: GitlabConfig -> IO [Mr]
getMRs config = do
    r <- getBody config
    let allItems subLens = toListOf (responseBody . values . subLens) r
        title = key "title" . _String
        stuff = (1, 2, 3) ^.. each
        zipped = zip (allItems title) (allItems link)
    print stuff
    return zipped

link :: Traversal' Value Text
link = key "web_url" . _String

assignee :: Traversal' Value Text
assignee = key "assignee" . key "name" . _String

getBody :: GitlabConfig -> IO (Response LBS.ByteString)
getBody (GitlabConfig host token group) =
    let bsToken = C.pack token
        opts = defaults & header "Accept" .~ ["application/json"] & header "authorization" .~ ["Bearer " <> bsToken]
        url = "https://" <> host <> "/api/v4/groups/" <> group <> "/merge_requests?state=opened"
     in getWith opts url
