branch-bot
==========

What?
-----

It's a Slack bot that talks about Gitlab branches and open MRs.


Why?
----

I got bored of doing it.


How?
----

Using: 
 * Haskell (GHC 8.x)
 * A [forked `slack-api`](https://github.com/declension/slack-api) for the [Slack RTM API](https://api.slack.com/rtm).
 * The [Gitlab v4 REST API](https://docs.gitlab.com/ee/api/)
